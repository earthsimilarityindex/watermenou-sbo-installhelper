#!/bin/sh

SOFTWARE_NAME_01="libopenglrecorder"
SOFTWARE_VERSION_01="0.1.0"
SOFTWARE_SOURCE_URL_01="https://github.com/Benau/libopenglrecorder/archive/v0.1.0/libopenglrecorder-0.1.0.tar.gz"
SOFTWARE_ARCHIVE_NAME_01=$(basename $SOFTWARE_SOURCE_URL_01)
SOFTWARE_ARCHIVE_MD5SUM_01="ddd91c2859b285366299257e55150376"
SOFTWARE_README_01="https://slackbuilds.org/slackbuilds/14.2/libraries/libopenglrecorder/README"
SOFTWARE_README_PAGER_01="lynx"
SBO_PKG_LOG_NAME_01="libopenglrecorder-0.1.0-x86_64-1_SBo"
SBO_SOURCE_URL_01="https://slackbuilds.org/slackbuilds/14.2/libraries/libopenglrecorder.tar.gz"
SBO_GPG_VERIFY_01="false"

SOFTWARE_NAME_02="OpenAL"
SOFTWARE_VERSION_02="1.18.0"
SOFTWARE_SOURCE_URL_02="https://www.openal-soft.org/openal-releases/openal-soft-1.18.0.tar.bz2"
SOFTWARE_ARCHIVE_NAME_02=$(basename $SOFTWARE_SOURCE_URL_02)
SOFTWARE_ARCHIVE_MD5SUM_02="704d41343b52dd04115de2dcdac5de03"
SOFTWARE_README_02="https://slackbuilds.org/slackbuilds/14.2/libraries/OpenAL/README"
SOFTWARE_README_PAGER_02="lynx"
SBO_PKG_LOG_NAME_02="OpenAL-1.18.0-x86_64-1_SBo"
SBO_SOURCE_URL_02="https://slackbuilds.org/slackbuilds/14.2/libraries/OpenAL.tar.gz"
SBO_GPG_VERIFY_02="false"

SOFTWARE_NAME_03="supertuxkart"
SOFTWARE_VERSION_03="1.0"
SOFTWARE_SOURCE_URL_03="http://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/1.0/supertuxkart-1.0-src.tar.xz"
SOFTWARE_ARCHIVE_NAME_03=$(basename $SOFTWARE_SOURCE_URL_03)
SOFTWARE_ARCHIVE_MD5SUM_03="3059ea9c146b50f40f2b59a606da0fc0"
SOFTWARE_README_03="https://slackbuilds.org/slackbuilds/14.2/games/supertuxkart/README"
SOFTWARE_README_PAGER_03="lynx"
SBO_PKG_LOG_NAME_03="supertuxkart-1.0-x86_64-1_SBo"
SBO_SOURCE_URL_03="https://slackbuilds.org/slackbuilds/14.2/games/supertuxkart.tar.gz"
SBO_GPG_VERIFY_03="false"

BUILD_ORDER="01 02 03"

# build option for supertuxkart
export RECORDER=yes

checksum_md5sum () {
  tempfile=$(mktemp)
  md5sum_data="$2  /root/Downloads/$1/$3"
  echo "$md5sum_data" >"$tempfile"
  unset md5sum_data
  md5sum -c "$tempfile" || exit
  rm "$tempfile" || exit
  unset tempfile
}

sbo_builder () {
  if test -f "/var/log/packages/$1" ; then
    echo "$2 $3 is already installed."
    return 1
  else
    test -d "/root/Downloads/$2" && rm -r "/root/Downloads/$2"
    rm "/root/Downloads/$2.tar.gz" || rm -r "/root/Downloads/$2.tar.gz"
    wget "$4" || exit
    if test "$5" = true ; then
      wget "$4.asc" || exit
      gpg --verify "/root/Downloads/$2.tar.gz.asc" "/root/Downloads/$2.tar.gz" || exit
    fi
    tar zxf "/root/Downloads/$2.tar.gz"
    cd "/root/Downloads/$2" || exit
    wget "$6" || exit
    checksum_md5sum "$2" "$7" "$8"
    "/root/Downloads/$2/$2.SlackBuild"
    upgradepkg "/tmp/$1.tgz" || installpkg "/tmp/$1.tgz"
    ldconfig
    cp "/tmp/$1.tgz" /root/Downloads/
    cd /root/Downloads
    return 0
  fi
}

if test ! "$USER" = root ; then
  echo "Run this script as root, bye."
  exit
else
  cd /root || exit
  . /etc/profile || exit
  test -d /root/Downloads || mkdir /root/Downloads
  cd /root/Downloads
  unset i
  for i in $BUILD_ORDER ; do
    eval sbo_builder \
      "\$SBO_PKG_LOG_NAME_$i" \
      "\$SOFTWARE_NAME_$i" \
      "\$SOFTWARE_VERSION_$i" \
      "\$SBO_SOURCE_URL_$i" \
      "\$SBO_GPG_VERIFY_$i" \
      "\$SOFTWARE_SOURCE_URL_$i" \
      "\$SOFTWARE_ARCHIVE_MD5SUM_$i" \
      "\$SOFTWARE_ARCHIVE_NAME_$i"
  done
  if test "$?" = 0 ; then
    unset i
    for i in $BUILD_ORDER ; do
      eval \$SOFTWARE_README_PAGER_$i "\$SOFTWARE_README_$i"
    done
    unset i
  fi
fi