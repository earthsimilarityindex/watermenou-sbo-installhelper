#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SOFTWARE_NAME_01="fontforge"
SOFTWARE_VERSION_01="20190317"
SOFTWARE_SOURCE_URLS_01=$(cat <<EOS
http://unrealize.co.uk/source/cidmaps.tgz
https://github.com/fontforge/fontforge/archive/20190317/fontforge-20190317.tar.gz
https://mirrors.slackware.com/slackware/slackware-current/source/l/freetype/freetype-2.10.1.tar.xz
http://http.debian.net/debian/pool/main/g/gnulib/gnulib_20180621~6979c25.orig.tar.gz
https://github.com/troydhanson/uthash/archive/v2.1.0/uthash-2.1.0.tar.gz
EOS
)
SOFTWARE_SOURCE_MD5SUMS_AND_NAMES_01=$(cat <<EOS
063691163e592515b31514515bb3ce8c cidmaps.tgz
e461181fb446b90b96fcb773da6d87ce fontforge-20190317.tar.gz
bd42e75127f8431923679480efb5ba8f freetype-2.10.1.tar.xz
508cba1d9bf589eefe2979a21a41e80e gnulib_20180621~6979c25.orig.tar.gz
4d0a33f6393260926032f1fad4bad39a uthash-2.1.0.tar.gz
EOS
)
SOFTWARE_README_01="index.html"
SOFTWARE_README_PAGER_01="lynx"
SBO_PKG_LOG_NAME_01="fontforge-20190317-x86_64-1_SBo"
SBO_SOURCE_URL_01="https://slackbuilds.org/slackbuilds/14.2/graphics/fontforge.tar.gz"
SBO_GPG_VERIFY_01="false"
SBO_SLACKBUILD_PATCH_01=$(cat <<EOS
mv /root/Downloads/fontforge/fontforge.SlackBuild /root/Downloads/fontforge/fontforge.SlackBuild.orig
sed "s/2.10.0/2.10.1/" /root/Downloads/fontforge/fontforge.SlackBuild.orig >/root/Downloads/fontforge/fontforge.SlackBuild
chmod +x /root/Downloads/fontforge/fontforge.SlackBuild
EOS
)

checksum_md5sum () {
  tempfile=$(mktemp)
  md5sum_data="$2  /root/Downloads/$1/$3"
  echo "$md5sum_data" >"$tempfile"
  unset md5sum_data
  md5sum -c "$tempfile" || exit
  rm "$tempfile" || exit
  unset tempfile
}

sbo_builder () {
  if test -f "/var/log/packages/$1" ; then
    echo "$2 $3 is already installed."
    return 1
  else
    test -d "/root/Downloads/$2" && rm -r "/root/Downloads/$2"
    rm "/root/Downloads/$2.tar.gz" || rm -r "/root/Downloads/$2.tar.gz"
    wget "$4" || exit
    if test "$5" = true ; then
      wget "$4.asc" || exit
      gpg --verify "/root/Downloads/$2.tar.gz.asc" "/root/Downloads/$2.tar.gz" || exit
    fi
    tar zxf "/root/Downloads/$2.tar.gz"
    cd "/root/Downloads/$2" || exit
    . "$8"
    cat "$6" | while read line; do
        wget "$line" || exit
    done
    unset line
    cat "$7" | while read line; do
        checksum_md5sum "$2" $line
    done
    unset line
    "/root/Downloads/$2/$2.SlackBuild"
    upgradepkg "/tmp/$1.tgz" || installpkg "/tmp/$1.tgz"
    ldconfig
    cp "/tmp/$1.tgz" /root/Downloads/
    cd /root/Downloads
    return 0
  fi
}

test -L /var && exit
test -L /var/log && exit
test -L /var/log/packages && exit
test -L /tmp && exit
test -L /root && exit
test -L /root/Downloads && exit

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit
else
  cd /root || exit
  . /etc/profile || exit
  test -d /root/Downloads || mkdir /root/Downloads
  cd /root/Downloads
    tempfile_SOFTWARE_SOURCE_URLS=$(mktemp)
    tempfile_SOFTWARE_SOURCE_MD5SUMS_AND_NAMES=$(mktemp)
    tempfile_SBO_SLACKBUILD_PATCH=$(mktemp)
    echo "$SOFTWARE_SOURCE_URLS_01" >"$tempfile_SOFTWARE_SOURCE_URLS"
    echo "$SOFTWARE_SOURCE_MD5SUMS_AND_NAMES_01" >"$tempfile_SOFTWARE_SOURCE_MD5SUMS_AND_NAMES"
    echo "$SBO_SLACKBUILD_PATCH_01" >"$tempfile_SBO_SLACKBUILD_PATCH"
    sbo_builder \
      "$SBO_PKG_LOG_NAME_01" \
      "$SOFTWARE_NAME_01" \
      "$SOFTWARE_VERSION_01" \
      "$SBO_SOURCE_URL_01" \
      "$SBO_GPG_VERIFY_01" \
      "$tempfile_SOFTWARE_SOURCE_URLS" \
      "$tempfile_SOFTWARE_SOURCE_MD5SUMS_AND_NAMES" \
      "$tempfile_SBO_SLACKBUILD_PATCH"
  if test "$?" = 0; then
    $SOFTWARE_README_PAGER_01 "/usr/doc/$SOFTWARE_NAME_01-$SOFTWARE_VERSION_01/$SOFTWARE_README_01"
  fi
  rm "$tempfile_SOFTWARE_SOURCE_URLS" "$tempfile_SOFTWARE_SOURCE_MD5SUMS_AND_NAMES" "$tempfile_SBO_SLACKBUILD_PATCH"
  unset tempfile_SOFTWARE_SOURCE_URLS tempfile_SOFTWARE_SOURCE_MD5SUMS_AND_NAMES tempfile_SBO_SLACKBUILD_PATCH
fi
