#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark 
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

MD5SUM_SBOTOOLS_27="ddf4b174fa29839564d7e784ff142581  /root/Downloads/sbotools/sbotools-2.7.tar.gz"

checksum_md5sum () {
	timestamp=$(date +%Y%m%d%H%M%S%N)
	rm -f /tmp/md5sum-$1.$timestamp
	echo $2 >/tmp/md5sum-$1.$timestamp
	chmod go-r /tmp/md5sum-$1.$timestamp
	echo "Check MD5 hash value."
	md5sum -c /tmp/md5sum-$1.$timestamp || exit
	echo
	rm /tmp/md5sum-$1.$timestamp || exit
	unset timestamp
}

if test ! $USER = root ; then
	echo "Run this script as root, bye."
	exit
else
	cd /root || exit
	. /etc/profile
	test -d /root/Downloads || mkdir /root/Downloads
	cd /root/Downloads
	if test -f /var/log/packages/sbotools-2.7-noarch-1_SBo ; then
		echo "sbotools 2.7 is already installed."
	else
		test -d /root/Downloads/sbotools &&
			rm -r /root/Downloads/sbotools
		wget https://slackbuilds.org/slackbuilds/14.2/system/sbotools.tar.gz \
			-O /root/Downloads/sbotools.tar.gz || exit
#		wget https://slackbuilds.org/slackbuilds/14.2/system/sbotools.tar.gz.asc \
#			-O /root/Downloads/sbotools.tar.gz.asc || exit
#		gpg --verify /root/Downloads/sbotools.tar.gz.asc /root/Downloads/sbotools.tar.gz || exit
		tar zxf /root/Downloads/sbotools.tar.gz
		cd /root/Downloads/sbotools
		wget https://pink-mist.github.io/sbotools/downloads/sbotools-2.7.tar.gz || exit
		checksum_md5sum sbotools27 "$MD5SUM_SBOTOOLS_27"
		/root/Downloads/sbotools/sbotools.SlackBuild
		upgradepkg /tmp/sbotools-2.7-noarch-1_SBo.tgz ||
			installpkg /tmp/sbotools-2.7-noarch-1_SBo.tgz
		ldconfig
		cp /tmp/sbotools-2.7-noarch-1_SBo.tgz /root/Downloads/
		cd /root/Downloads
		less /var/log/packages/sbotools-2.7-noarch-*_SBo
	fi
fi