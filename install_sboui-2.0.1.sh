#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

MD5SUM_LIBCONFIG_172="d666f0ca093906d34d6e1e4890053e62  /root/Downloads/libconfig/libconfig-1.7.2.tar.gz"
MD5SUM_SBOUI_201="2a0170abe92d3505f116fae9b2029543  /root/Downloads/sboui/sboui-2.0.1.tar.gz"

checksum_md5sum () {
	timestamp=$(date +%Y%m%d%H%M%S%N)
	rm -f /tmp/md5sum-$1.$timestamp
	echo $2 >/tmp/md5sum-$1.$timestamp
	chmod go-r /tmp/md5sum-$1.$timestamp
	echo "Check MD5 hash value."
	md5sum -c /tmp/md5sum-$1.$timestamp || exit
	echo
	rm /tmp/md5sum-$1.$timestamp || exit
	unset timestamp
}

if test ! $USER = root ; then
    echo "Run this script as root, bye."
    exit
else
    cd /root || exit
    . /etc/profile
    test -d /root/Downloads || mkdir /root/Downloads
    cd /root/Downloads
    if test -f /var/log/packages/libconfig-1.7.2-x86_64-1_SBo ; then
        echo "libconfig 1.7.2 is already installed."
    else
        test -d /root/Downloads/libconfig && rm -r /root/Downloads/libconfig
        wget https://slackbuilds.org/slackbuilds/14.2/libraries/libconfig.tar.gz \
            -O /root/Downloads/libconfig.tar.gz || exit
#        wget https://slackbuilds.org/slackbuilds/14.2/libraries/libconfig.tar.gz.asc \
#            -O /root/Downloads/libconfig.tar.gz.asc || exit
#        gpg --verify /root/Downloads/libconfig.tar.gz.asc /root/Downloads/libconfig.tar.gz || exit
        tar zxf /root/Downloads/libconfig.tar.gz
        cd /root/Downloads/libconfig
        wget https://github.com/hyperrealm/libconfig/archive/v1.7.2/libconfig-1.7.2.tar.gz || exit
        checksum_md5sum libconfig172 "$MD5SUM_LIBCONFIG_172"
        /root/Downloads/libconfig/libconfig.SlackBuild
        upgradepkg /tmp/libconfig-1.7.2-x86_64-1_SBo.tgz ||
            installpkg /tmp/libconfig-1.7.2-x86_64-1_SBo.tgz
        ldconfig
        cp /tmp/libconfig-1.7.2-x86_64-1_SBo.tgz /root/Downloads/
        cd /root/Downloads
    fi
    if test -f /var/log/packages/sboui-2.0.1-x86_64-1_SBo ; then
        echo "sboui 2.0.1 is already installed."
    else
        test -d /root/Downloads/sboui && rm -r /root/Downloads/sboui
        wget https://slackbuilds.org/slackbuilds/14.2/system/sboui.tar.gz \
            -O /root/Downloads/sboui.tar.gz || exit
#        wget https://slackbuilds.org/slackbuilds/14.2/system/sboui.tar.gz.asc \
#            -O /root/Downloads/sboui.tar.gz.asc || exit
#        gpg --verify /root/Downloads/sboui.tar.gz.asc /root/Downloads/sboui.tar.gz || exit
        tar zxf /root/Downloads/sboui.tar.gz
        cd /root/Downloads/sboui
        wget https://github.com/montagdude/sboui/archive/2.0.1/sboui-2.0.1.tar.gz || exit
        checksum_md5sum sboui201 "$MD5SUM_SBOUI_201"
        /root/Downloads/sboui/sboui.SlackBuild
        upgradepkg /tmp/sboui-2.0.1-x86_64-1_SBo.tgz ||
            installpkg /tmp/sboui-2.0.1-x86_64-1_SBo.tgz
        ldconfig
        cp /tmp/sboui-2.0.1-x86_64-1_SBo.tgz /root/Downloads/
        cd /root/Downloads
        less /usr/doc/libconfig-1.7.2/README
        less /usr/doc/sboui-2.0.1/README.md
    fi
fi
