#!/bin/sh

SOFTWARE_NAME_01="xcowsay"
SOFTWARE_VERSION_01="1.4"
SOFTWARE_SOURCE_URL_01="https://github.com/nickg/xcowsay/releases/download/v1.4/xcowsay-1.4.tar.gz"
SOFTWARE_ARCHIVE_NAME_01="xcowsay-1.4.tar.gz"
SOFTWARE_ARCHIVE_MD5SUM_01="1e9933c6f48f1e8fffbeaeb6ba0e2bc8"
SOFTWARE_README_01="https://slackbuilds.org/slackbuilds/14.2/games/xcowsay/README"
SOFTWARE_README_PAGER_01="lynx"
SBO_PKG_LOG_NAME_01="xcowsay-1.4-x86_64-1_SBo"
SBO_SOURCE_URL_01="https://slackbuilds.org/slackbuilds/14.2/games/xcowsay.tar.gz"
SBO_GPG_VERIFY_01="false"

BUILD_ORDER="01"

checksum_md5sum () {
  tempfile=$(mktemp)
  md5sum_data="$2  /root/Downloads/$1/$3"
  echo "$md5sum_data" >"$tempfile"
  unset md5sum_data
  md5sum -c "$tempfile" || exit
  rm "$tempfile" || exit
  unset tempfile
}

sbo_builder () {
  if test -f "/var/log/packages/$1" ; then
    echo "$2 $3 is already installed."
    return 1
  else
    test -d "/root/Downloads/$2" && rm -r "/root/Downloads/$2"
    rm "/root/Downloads/$2.tar.gz" || rm -r "/root/Downloads/$2.tar.gz"
    wget "$4" || exit
    if test "$5" = true ; then
      wget "$4.asc" || exit
      gpg --verify "/root/Downloads/$2.tar.gz.asc" "/root/Downloads/$2.tar.gz" || exit
    fi
    tar zxf "/root/Downloads/$2.tar.gz"
    cd "/root/Downloads/$2" || exit
    wget "$6" || exit
    checksum_md5sum "$2" "$7" "$8"
    "/root/Downloads/$2/$2.SlackBuild"
    upgradepkg "/tmp/$1.tgz" || installpkg "/tmp/$1.tgz"
    ldconfig
    cp "/tmp/$1.tgz" /root/Downloads/
    cd /root/Downloads
    return 0
  fi
}

if test ! "$USER" = root ; then
  echo "Run this script as root, bye."
  exit
else
  cd /root || exit
  . /etc/profile || exit
  test -d /root/Downloads || mkdir /root/Downloads
  cd /root/Downloads
  unset i
  for i in $BUILD_ORDER ; do
    eval sbo_builder \
      "\$SBO_PKG_LOG_NAME_$i" \
      "\$SOFTWARE_NAME_$i" \
      "\$SOFTWARE_VERSION_$i" \
      "\$SBO_SOURCE_URL_$i" \
      "\$SBO_GPG_VERIFY_$i" \
      "\$SOFTWARE_SOURCE_URL_$i" \
      "\$SOFTWARE_ARCHIVE_MD5SUM_$i" \
      "\$SOFTWARE_ARCHIVE_NAME_$i"
  done
  if test "$?" = 0 ; then
    unset i
    for i in $BUILD_ORDER ; do
      eval \$SOFTWARE_README_PAGER_$i "\$SOFTWARE_README_$i"
    done
    unset i
  fi
fi